{
  kapack ? import
    (builtins.fetchTarball {
      url = "https://github.com/oar-team/kapack/archive/master.tar.gz";
    }) {}
}:

kapack.pkgs.mkShell rec {
  buildInputs = with kapack; with pkgs; [
    batsim
    batexpe
    pkgconfig
    zeromq
    cargo
    rustc
  ];
}
