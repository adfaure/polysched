{ stdenv, fetchFromGitHub, rustPlatform, makeWrapper, zeromq, pkgconfig}:

with rustPlatform;

buildRustPackage rec {
  name = "evi-${version}";
  version = "1.1.0";

  propagatedBuildInputs = [
    pkgconfig
    zeromq
  ];

  src = ./.;
  depsSha256 = "03lsxhn92ivrw73g6i9gwk2qyvvyfckcglzcyq1pfdjmgqrhypz6";
  cargoSha256 = "1qhqdbsywbjg0ckwsshk1q5qk8imj02rbfccmn84h829v3kmvck7";
}
