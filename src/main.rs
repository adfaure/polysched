#[macro_use]
extern crate log;
extern crate env_logger;

#[macro_use]
extern crate serde_derive;
extern crate docopt;

use docopt::Docopt;

extern crate batsim;

#[macro_use]
extern crate serde_json;

mod redirection;
mod fcfs;

use redirection::{broker, scheduler};

static VERSION: &'static str = "1.2.3";
const USAGE: &'static str = "
Usage:
    polys --version
    polys [ --port=<port> ] [ --easybackfilling | --help]
    polys redirection [ --port=<port> --redirection-easybackfilling --easybackfilling ] \
          --threshold=<th> \
          --redirection-allocation=<p> \
          [ --conflict-policy=<cpolicy> ] \
          [ --threshold-policy=<tpolicy> ] \
          [ --threshold-filter-policy=<tfpolicy> ]

    polys redirection --help
    polys --help

Examples:
    polys -e
    polys redirection -eb --threshold 1 --redirection-allocation 0.3

Options:
    -h --help                                       Show this screen.
    -v --version                                    Print version.
    -e --easybackfilling                            Use easy backfilling.
    -s --port=<port>                                The port number to bind the socket on.

Redirection Options:
    -e --easybackfilling                            Use easy backfilling on the jobs
                                                    that have been redirected.

    -b --redirection-easybackfilling                Activate easybackfilling to the
                                                    redirection scheduler.

    -t --threshold=<th>                             Set threshold of redirection.
    -T --threshold-policy=<tpolicy>                 Set the way the threshold
                                                    is computed possible values are
                                                    walltime, area, count, resources.

    -F --threshold-filter-policy=<tfpolicy>          Set the function that filter
                                                    jobs before increasing the pressure.

    -p --conflict-policy=<cpolicy>                  Set policy of conflicts. Possible values are
                                                    longest_job, greatest_area, most_resources.

    -a --redirection-allocation=<p>                 Size in percentage allocated for
                                                    the redirected jobs.
";

#[derive(Debug, Deserialize)]
struct Args {
    cmd_redirection: bool,
    flag_version: bool,
    flag_easybackfilling: bool,
    // Option for the redirection scheduler
    flag_port: Option<i64>,
    flag_threshold: Option<f64>,
    flag_redirection_allocation: Option<f64>,
    flag_conflict_policy: Option<String>,
    flag_threshold_policy: Option<String>,
    flag_threshold_filter_policy: Option<String>,
    flag_redirection_easybackfilling: bool,
}

fn main() {
    env_logger::init().unwrap();
    info!("starting up");

    let args: Args = Docopt::new(USAGE)
        .and_then(|d| d.deserialize())
        .unwrap_or_else(|e| e.exit());


    if args.flag_version {
        println!("{}", VERSION);
        return;
    }

    match args.flag_port {
        Some(1 ... 1023) => warn!("Port specified needs root privileges"),
        Some(port) => {
            if port > 65535 {
                panic!("Tcp port not available (max is 65535, given: {})", port)
            }
        },
        None => {}
    }

    if args.cmd_redirection {

        info!(
            "Initialize scheduler with \
              redirection (easybackfilling={}, threshold={:?})",
            args.flag_redirection_easybackfilling,
            args.flag_threshold
        );

        info!("Port specified: {:?}", args.flag_port);

        // In fact, due to the uses of docopt
        // all the case that I am checking should
        // be detect id the command is wrong.

        // Get the value of the redirection threshold.
        let th = args.flag_threshold
            .ok_or("--threshold must be set with.")
            .unwrap();

        let p = args.flag_redirection_allocation
            .ok_or("--redirection-allocation must be set with.")
            .unwrap();

        if p < 0.0 || p > 1.0 {
            panic!(
                "The allocation percentage must be \
                   specified in between 0.0 and 1.0"
            );
        }

        // Possible values are: most_resources, longest_job, greatest_area
        let cpolicy = args.flag_conflict_policy.unwrap_or(String::from("most_resources"));
        info!("The conflict policy is set to :{:?}", cpolicy);

        // Possible values are: count, area
        let tpolicy = args.flag_threshold_policy.unwrap_or(String::from("count"));
        info!("The threshold policy is set to :{:?}", tpolicy);

        // Possible values are: count, area
        let tfpolicy = args.flag_threshold_filter_policy.unwrap_or(tpolicy.clone());
        info!("The threshold filter policy is set to :{:?}", tfpolicy);


        // Scheduler for the job that can apply redirection.
        let mut redirection_scheduler = scheduler::PolyScheduler::new(
            th,
            args.flag_redirection_easybackfilling,
            cpolicy,
            tpolicy,
            tfpolicy,
        );

        // Initialize the scheduler for the redirected jobs
        let mut scheduler = fcfs::FCFS::new(args.flag_easybackfilling);
        // Initialize the broker that is responsible for dispatching the
        // jobs to the scheduler they belong to.
        // At the initialization, the broker splits the cluster according to
        // the allocation of each scheduler size, and allocates the resources.
        let mut broker = broker::Broker::new(&mut redirection_scheduler, &mut scheduler, p);

        // Initialize the batsim
        let mut batsim = batsim::Batsim::new(&mut broker, args.flag_port);
        // Run the simulation
        batsim.run_simulation().unwrap();

    } else {

        info!(
            "Initialize scheduler (easybackfilling={}), specified port: {:?}",
            args.flag_easybackfilling,
            args.flag_port
        );

        let mut scheduler = fcfs::FCFS::new(args.flag_easybackfilling);
        let mut batsim = batsim::Batsim::new(&mut scheduler, args.flag_port);
        batsim.run_simulation().unwrap();

    }
    info!("Simulation finished");
}
