extern crate serde_json;
extern crate interval_set;

use self::interval_set::{Interval, ToIntervalSet};

use batsim::*;

pub struct Broker<'a> {
    // Scheduler that handles almost the whole cluster and
    // use evictions policies
    rejection_scheduler: &'a mut Scheduler,
    // Scheduler that schedules all redirected jobs
    scheduler: &'a mut Scheduler,
    // size in percentage for the redirection part
    redirection_percetage_allocation: f64,
}

impl<'a> Broker<'a> {
    pub fn new(
        redsched: &'a mut Scheduler,
        sched: &'a mut Scheduler,
        redirection_percetage_allocation: f64,
    ) -> Broker<'a> {
        Broker {
            rejection_scheduler: redsched,
            scheduler: sched,
            redirection_percetage_allocation: redirection_percetage_allocation,
        }
    }
}

impl<'a> Scheduler for Broker<'a> {
    fn simulation_begins(
        &mut self,
        timestamp: &f64,
        nb_resources: i32,
        config: serde_json::Value,
    ) -> Option<Vec<BatsimEvent>> {

        // We tell batsim that it does not need to wait for us
        let rejection_percentage = self.redirection_percetage_allocation;

        // Wet compute the resources that will be given to
        // each schedulers according to the percentage of
        // the machine allocated for the rejection

        // size for the part of the cluster that will
        // have rejection policy
        let mut conf_redir = config.clone();
        let size_redir = nb_resources - (nb_resources as f64 * rejection_percentage).ceil() as i32;
        let resources_redirection = Interval::new(0 as u32, size_redir as u32 - 1)
            .to_interval_set();

        // size of the cluster that will be dedicated to
        // job that are redirected
        let mut conf_regular = config.clone();
        let size_regular = nb_resources - size_redir;
        let resources_regular = Interval::new(size_redir as u32, nb_resources as u32 - 1)
            .to_interval_set();

        // We update the configuration that will be given to
        // the sub schedulers
        conf_redir["resources"] = serde_json::Value::String(resources_redirection.to_string());
        conf_redir["size_for_redirection"] =
            serde_json::Value::Number(serde_json::Number::from(size_regular as i64));

        conf_regular["resources"] = serde_json::Value::String(resources_regular.to_string());

        self.rejection_scheduler.simulation_begins(
            timestamp,
            size_redir as i32,
            conf_redir,
        );
        self.scheduler.simulation_begins(
            timestamp,
            size_regular as i32,
            conf_regular,
        );
        trace!(
            "Allocate-{}/{} red/regular. Total={}",
            size_redir,
            size_regular,
            nb_resources
        );
        Some(vec![
            notify_event(
                *timestamp,
                String::from("submission_finished")
            ),
        ])
    }

    fn on_job_submission(
        &mut self,
        timestamp: &f64,
        job: Job,
        profile: Option<Profile>,
    ) -> Option<Vec<BatsimEvent>> {
        let (w_id, _) = Job::split_id(&job.id);

        // We dispatch the job to the right scheduler
        // and we submit its reply to batsim
        match w_id.as_ref() {
            "rej!" => self.scheduler.on_job_submission(timestamp, job, profile),
            _ => {
                self.rejection_scheduler.on_job_submission(
                    timestamp,
                    job,
                    profile,
                )
            }
        }
    }

    fn on_job_completed(
        &mut self,
        timestamp: &f64,
        job_id: String,
        job_state: String,
        return_code: i32,
        alloc: String
    ) -> Option<Vec<BatsimEvent>> {
        trace!("Job={} terminated with Status: {}, return_code: {}", job_id, job_state, return_code);
        let (w_id, _) = Job::split_id(&job_id);

        // We dispatch the job to the right scheduler
        // and we submit its reply to batsim
        match w_id.as_ref() {
            "rej!" => self.scheduler.on_job_completed(timestamp, job_id, job_state, return_code, alloc),
            _ => {
                self.rejection_scheduler.on_job_completed(
                    timestamp,
                    job_id,
                    job_state,
                    return_code,
                    alloc
                )
            }
        }
    }

    fn on_simulation_ends(&mut self, timestamp: &f64) {
        self.rejection_scheduler.on_simulation_ends(timestamp);
        self.scheduler.on_simulation_ends(timestamp);
        println!("Simulation ends: {}", timestamp);
    }

    fn on_job_killed(&mut self, timestamp: &f64, job_ids: Vec<String>) -> Option<Vec<BatsimEvent>> {
        let mut events: Vec<BatsimEvent> = vec![];

        // Get all jobs that come from the rejected workload
        let rej_jobs: Vec<String> = job_ids
            .iter()
            .filter(|id| {
                let (w_id, _) = Job::split_id(&id);
                w_id == "rej!"
            })
            .map(|id| id.clone())
            .collect::<Vec<_>>();

        let regular_jobs: Vec<String> = job_ids
            .iter()
            .filter(|id| {
                let (w_id, _) = Job::split_id(&id);
                w_id != "rej!"
            })
            .map(|id| id.clone())
            .collect::<Vec<_>>();


        // Call the scheduler that can reject jobs
        if regular_jobs.len() > 0 {
            match self.rejection_scheduler.on_job_killed(
                timestamp,
                regular_jobs,
            ) {
                Some(mut es) => events.append(&mut es),
                None => {}
            };
        }

        if rej_jobs.len() > 0 {
            // Call the scheduler that is not allowed to reject jobs
            match self.scheduler.on_job_killed(timestamp, rej_jobs) {
                Some(mut es) => events.append(&mut es),
                None => {}
            };
        }

        if events.is_empty() {
            return None;
        }
        Some(events)
    }

    fn on_message_received_end(&mut self, timestamp: &mut f64) -> Option<Vec<BatsimEvent>> {
        let mut events: Vec<BatsimEvent> = vec![];

        // Call the scheduler that can reject jobs
        match self.rejection_scheduler.on_message_received_end(timestamp) {
            Some(mut es) => events.append(&mut es),
            None => {}
        };
        // Call the scheduler that is not allowed to reject jobs
        match self.scheduler.on_message_received_end(timestamp) {
            Some(mut es) => events.append(&mut es),
            None => {}
        };

        if events.is_empty() {
            return None;
        }
        Some(events)
    }

    fn on_message_received_begin(&mut self, timestamp: &f64) -> Option<Vec<BatsimEvent>> {
        let mut events: Vec<BatsimEvent> = vec![];

        // Call the scheduler that can reject jobs
        match self.rejection_scheduler.on_message_received_begin(
            timestamp,
        ) {
            Some(mut es) => events.append(&mut es),
            None => {}
        };
        // Call the scheduler that is not allowed to reject jobs
        match self.scheduler.on_message_received_begin(timestamp) {
            Some(mut es) => events.append(&mut es),
            None => {}
        };

        if events.is_empty() {
            return None;
        }
        Some(events)
    }
}
