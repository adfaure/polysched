extern crate serde_json;
extern crate interval_set;

use std::cmp::Ordering;
use std::collections::BinaryHeap;
use self::interval_set::{Interval, IntervalSet, ToIntervalSet};
use batsim::*;
use std::collections::LinkedList;
use std::collections::HashMap;
use redirection::utils::{construct_rejected_job};
use std::collections::hash_map::Entry;

struct RunningJob<'a>(&'a IntervalSet, &'a f64);

#[derive(Copy,Clone)]
pub enum ThresholdPolicy {
    Count,
    Area,
    Resources,
    Walltime
}

/// Poly scheduler can be used for various
/// kind of scheduling.
pub struct PolyScheduler {
    // Is easy backfilling activated
    pub easy_backfilling: bool,
    pub kills_pending: HashMap<String, (Job, bool)>,
    // nb total of resources
    pub nb_resources: u32,
    // A mutable interval set of
    // resources to hold the current state
    // of the system.
    pub resources: IntervalSet,
    pub time: f64,
    // Job that are currently running
    // We keep, the job, its resources and the
    // time at which it must finished.
    // The last parameter is the pressure compter
    pub running_jobs: HashMap<String, (Job, IntervalSet, f64, f64)>,
    // Current job queue
    pub job_queue: LinkedList<Job>,
    // Initial configuration
    pub config: serde_json::Value,
    // Threshold for the rejection
    // if the pressure overshoot this value
    // some jobs may be rejected.
    pub rejection_threshold: f64,
    // The size of the redirection part of
    // the cluster; to limit the target selection.
    pub redirection_size: i64,
    // The policy to decide which job
    // we will redirect in case multiple jobs
    // reach the trheshold
    pub cpolicy: fn(&Job, &Job) -> Ordering,
    // What is the function that we use to
    // keep track of the pressure of the queue
    pub tpolicy: ThresholdPolicy,
    pub tfpolicy: ThresholdPolicy,
    pub profiles: HashMap<String, Profile>,
}

impl PolyScheduler {
    pub fn new(
        redirection_threshold: f64,
        easy_backfilling: bool,
        cpolicy: String,
        tpolicy: String,
        tfpolicy: String
    ) -> PolyScheduler {

        let cpolicy = match cpolicy.as_ref() {
            "greatest_area" => greatest_area,
            "longest_job" => longest_job,
            "most_resources" => most_resources,
            _ => panic!("invalid policy: {:?}", cpolicy),
        };

        let tpolicy = match tpolicy.as_ref() {
            "count" => ThresholdPolicy::Count,
            "area" => ThresholdPolicy::Area,
            "resources" => ThresholdPolicy::Resources,
            "walltime" => ThresholdPolicy::Walltime,
            _ => panic!("invalid policy: {:?}", tpolicy),
        };

        let tfpolicy = match tfpolicy.as_ref() {
            "count" => ThresholdPolicy::Count,
            "area" => ThresholdPolicy::Area,
            "resources" => ThresholdPolicy::Resources,
            "walltime" => ThresholdPolicy::Walltime,
            _ => panic!("invalid policy: {:?}", tfpolicy),
        };

        PolyScheduler {
            nb_resources: 0,
            redirection_size: 0,
            time: 0.0,
            resources: IntervalSet::empty(),
            running_jobs: HashMap::new(),
            job_queue: LinkedList::new(),
            easy_backfilling: easy_backfilling,
            config: json!(null),
            kills_pending: HashMap::new(),
            rejection_threshold: redirection_threshold,
            cpolicy: cpolicy,
            tpolicy: tpolicy,
            tfpolicy: tfpolicy,
            profiles: HashMap::new(),
        }
    }

    fn schedule_jobs(&mut self, _: f64) -> Option<Vec<BatsimEvent>> {
        let mut res: Vec<BatsimEvent> = Vec::new();
        let mut optional = self.job_queue.pop_front();

        while let Some(job) = optional {
            match self.find_job_allocation(&self.resources, &job) {
                None => {
                    trace!("Cannot launch job={} now", job.id);
                    self.job_queue.push_front(job);
                    optional = None;
                }
                Some(allocation) => {
                    let alloc_str = format!("{}", allocation);
                    res.push(allocate_job_event(self.time, &job, alloc_str));
                    self.launch_job_internal(job.clone(), allocation.clone());
                    optional = self.job_queue.pop_front();
                }
            }
        }

        if self.easy_backfilling {
            let bf = self.easy_backfilling();
            for job in bf {
                // We allocate jobs that are ready for backfilling
                res.push(allocate_job_event(self.time, &job.0, format!("{}", job.1)));
                self.launch_job_internal(job.0.clone(), job.1.clone());
                // We find and remove backfilled jobs from the queue
                self.job_queue = self.job_queue
                    .iter()
                    .filter_map(|ref x| {
                        if x.id != job.0.id {
                            let clone = x.clone();
                            return Some(clone.clone());
                        }
                        None
                    })
                    .collect();
            }
        }
        Some(res)
    }

    fn easy_backfilling(&mut self) -> LinkedList<(Job, IntervalSet)> {
        let mut temporary_resources = self.resources.clone();
        let mut backfilled: LinkedList<(Job, IntervalSet)> = LinkedList::new();
        if 0 == self.resources.size() {
            return LinkedList::new();
        }

        match self.job_queue.front() {
            None => {}
            Some(job) => {
                let shadow = self.find_shadow_time(job.res as u64 - self.resources.size() as u64);
                for ref queued in self.job_queue.iter().skip(1) {
                    trace!(
                        "try to bf {} -- {} resources available",
                        queued,
                        temporary_resources.size()
                    );
                    let ends_at: f64 = self.time + queued.walltime;
                    if ends_at < shadow && queued.res <= temporary_resources.size() as i32 {
                        let allocation = self.find_job_allocation(&temporary_resources, queued)
                            .unwrap();
                        let job_clone = queued.clone();

                        backfilled.push_back((job_clone.clone(), allocation.clone()));
                        temporary_resources = temporary_resources.clone().difference(allocation);
                        trace!(
                            "bf {} -- {} resources available",
                            queued,
                            temporary_resources.size()
                        );
                    }
                    if temporary_resources.size() == 0 {
                        break;
                    }
                }
            }
        };
        backfilled
    }

    /// allocation_size: number of nodes to gather
    /// (it will not add the current number of free resources of the system).
    fn find_shadow_time(&self, allocation_size: u64) -> f64 {
        let mut priority_q = BinaryHeap::new();
        for (_, &(_, ref alloc, ref ends, _)) in &self.running_jobs {
            priority_q.push(RunningJob(alloc, ends))
        }

        let mut optional = priority_q.pop();
        let mut acc = 0u64;
        while let Some(i) = optional {
            acc += i.0.size() as u64;
            if acc >= allocation_size {
                return *i.1;
            }
            optional = priority_q.pop();
        }
        panic!(
            "Failed to get shadow time for {} more resources",
            allocation_size
        );
    }

    fn job_finished_internal(&mut self, job_id: String) {
        let (_, allocation, _, _) = self.running_jobs.remove(&job_id).unwrap();
        self.resources = self.resources.clone().union(allocation);
    }

    fn launch_job_internal(&mut self, job: Job, allocation: IntervalSet) {
        trace!("Launching Job::Job={} allocation={}", job.id, allocation);

        let term_time = self.time + job.walltime;
        self.resources = self.resources.clone().difference(allocation.clone());
        self.running_jobs.insert(
            job.id.clone(),
            (job, allocation, term_time, 0.0),
        );
    }

    fn find_job_allocation(&self, resources: &IntervalSet, job: &Job) -> Option<IntervalSet> {
        let current_available_size = resources.size();
        if current_available_size < (job.res as u32) {
            trace!(
                "No allocation possible yet for the job {} (nb res={}) (size available={})",
                job.id,
                job.res,
                current_available_size
            );
            return None;
        }

        trace!("Try to allocate Job={} res={}", job.id, job.res);
        let mut iter = resources.iter();
        let mut allocation = IntervalSet::empty();
        let mut left = job.res as u32;

        let mut interval = iter.next().unwrap();
        while allocation.size() != (job.res as u32) {
            // Note that we test earlier in the function if the interval
            // has enough resources, so this loop should not fail.
            let interval_size = interval.range_size();

            if interval_size > left {
                allocation.insert(Interval::new(
                    interval.get_inf(),
                    interval.get_inf() + left - 1,
                ));
            } else if interval_size == left {
                allocation.insert(interval.clone());
            } else if interval_size < left {
                allocation.insert(interval.clone());
                left -= interval_size;
                interval = iter.next().unwrap();
            }
        }
        Some(allocation)
    }
}

// The order is intentionally reversed because it is meant to be used into
// a priority queue, where the smallest time are have an higher priority.
impl<'a> Ord for RunningJob<'a> {
    fn cmp(&self, other: &Self) -> Ordering {
        if self.1 < other.1 {
            return Ordering::Greater;
        } else if self.1 == other.1 {
            if self.0.size() > other.0.size() {
                return Ordering::Greater;
            } else if self.0.size() == other.0.size() {
                return Ordering::Equal;
            }
        }
        return Ordering::Less;
    }
}


impl<'a> PartialOrd for RunningJob<'a> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl<'a> PartialEq for RunningJob<'a> {
    fn eq(&self, other: &Self) -> bool {
        self.1 == other.1
    }
}

impl<'a> Eq for RunningJob<'a> {}

impl Scheduler for PolyScheduler {
    fn simulation_begins(
        &mut self,
        timestamp: &f64,
        nb_resources: i32,
        config: serde_json::Value,
    ) -> Option<Vec<BatsimEvent>> {
        self.time = *timestamp;
        self.config = config;
        self.nb_resources = nb_resources as u32;

        if !&self.config["resources"].is_string() {
            panic!("Resource given by the broker has to be of String type");
        }

        if !&self.config["size_for_redirection"].is_i64() {
            panic!("The size of the redirection has to be a number(i64) type");
        }

        self.resources = String::from(self.config["resources"].as_str().unwrap()).to_interval_set();
        self.redirection_size = self.config["size_for_redirection"].as_i64().unwrap();

        info!(
            "Scheduler (easy bf:{}) Initialized with {} resources ({})",
            self.easy_backfilling,
            self.resources.size(),
            self.resources
        );
        info!("Threshold: {}",
            self.rejection_threshold,
        );
        info!("Max target size {}", self.redirection_size);
        // We tell batsim that it does not need to wait for us
        Some(vec![
            notify_event(
                *timestamp,
                String::from("submission_finished")
            ),
        ])
    }

    fn on_job_submission(
        &mut self,
        timestamp: &f64,
        job: Job,
        profile: Option<Profile>,
    ) -> Option<Vec<BatsimEvent>> {
        trace!("Received job(={})", job);
        trace!("profile(={:?})", job.profile);
        self.time = *timestamp;

        if job.res as usize > self.nb_resources as usize {
            trace!("Job(={}) is just too big", job);
            return Some(vec![reject_job_event(self.time, &job)]);
        }

        // We save the profile of the new job
        match profile {
            Some(p) => {
                self.profiles.insert(job.profile.clone(), p);
            }
            None => panic!("Did you forget to activate the profile forwarding ?"),
        }

        // If the queue is empty, why check if the job can directly run
        // otherwise we increase the redirection thresholds.
        if self.job_queue.is_empty() {
            match self.find_job_allocation(&self.resources, &job) {
                None => {
                    trace!("Cannot launch job={} now", job.id);
                },
                Some(allocation) => {
                    let alloc_str = format!("{}", allocation);
                    self.launch_job_internal(job.clone(), allocation.clone());
                    return Some(vec![allocate_job_event(self.time, &job, alloc_str)])
                }
            }
        }

        let mut res = None;


        let tpolicy: ThresholdPolicy = *(&self.tpolicy);
        let tfpolicy: ThresholdPolicy = *(&self.tfpolicy);

        // For each running job, we check whereas we should increase
        // its pressure according to the current policy.
        for running_job in self.running_jobs.iter_mut()
            .filter(|j| threshold_filter_policy(tfpolicy, &(j.1).0, &job)) {
            let value = running_job.1;
            value.3 += threshold_policy(tpolicy, &job);
            trace!("running job({:?} pressure {}", value.0, value.3);
        }

        // Once we increased all counters, we
        // look into the running jobs to detect
        // potential threshold overshoot.
        let target = self.running_jobs.iter().filter(
                |kv| (kv.1).3 >= self.rejection_threshold * threshold_policy(tpolicy, &(kv.1).0) &&
                    !self.kills_pending.contains_key(&(kv.1).0.id) &&
                    (kv.1).0.res <= self.redirection_size as i32)
                // Clone them so we are free to mute them
                .map(|x| (x.1).clone().0)
                // Ordering them according to the selected policy
                .min_by(
                    self.cpolicy
                );

        // If we find a target,
        // we send the kill to batsim.
        match target {
            None => {},
            Some(target_job) => {

                trace!("target found {:?}", target_job);
                self.kills_pending.insert(target_job.id.clone(), (target_job.clone(), false));
                res = Some(vec![kill_jobs_event(self.time, vec![&target_job])]);

                // From this point, we need to reset the counters of all jobs
                // that more or same resources than the freshly submitted job.
                let jobs = &mut self.running_jobs;

                jobs.iter_mut()
                    // .filter(|kv| (kv.1).0.res >= job.res)
                    .for_each(|kv| {
                                let alloc = kv.1;
                                alloc.3 = 0.0;
                            });
                },
        }

        self.job_queue.push_back(job);
        res
    }

    fn on_job_completed(
        &mut self,
        timestamp: &f64,
        job_id: String,
        job_state: String,
        return_code: i32,
        alloc: String,
        ) -> Option<Vec<BatsimEvent>> {
        self.time = *timestamp;

        match self.kills_pending.entry(job_id.clone()) {
            Entry::Occupied(mut o) => {
                o.get_mut().1 = true;
            }
            Entry::Vacant(_v) => {}
        };

        trace!("Job={} terminated with Status: {}, return code: {}", job_id, job_state, return_code);
        self.job_finished_internal(job_id);
       None
    }

    fn on_simulation_ends(&mut self, timestamp: &f64) {
        self.time = *timestamp;
        println!("Simulation ends: {}", timestamp);
    }

    fn on_job_killed(&mut self, timestamp: &f64, job_ids: Vec<String>) -> Option<Vec<BatsimEvent>> {
        trace!("The jobs has been successfully killed {:?}", job_ids);
        //self.kills_pending.contains_key()
        let mut events: Vec<BatsimEvent> = vec![];
        for id in job_ids {
            trace!("job(={}) killed", id);
            match self.kills_pending.remove(&id) {
                Some(killed_job) => {
                    if !killed_job.1 {
                        self.job_finished_internal(id);
                        let new_job = construct_rejected_job(killed_job.0);

                        let profile: Option<&Profile> = self.profiles.get(&new_job.profile);
                        events.push(submit_job_event(*timestamp, &new_job, profile));
                    } else {
                        trace!("Job(={:?} has been flagged as completed", killed_job.0);
                    }
                }
                None => {
                    panic!("job must have finished before kill occurred");
                }
            }
        }
        Some(events)
    }

    fn on_message_received_end(&mut self, timestamp: &mut f64) -> Option<Vec<BatsimEvent>> {
        trace!("Respond to batsim at: {}", timestamp);
        self.schedule_jobs(*timestamp)
    }

    fn on_message_received_begin(&mut self, timestamp: &f64) -> Option<Vec<BatsimEvent>> {
        trace!("Received new batsim message at {}", timestamp);
        None
    }
}


//
// Functions to select jobs depending on the conflict resolution policy
//
fn longest_job(a: &Job, b: &Job) -> Ordering {
    if a.walltime > b.walltime {
        return Ordering::Less;
    } else {
        return Ordering::Greater;
    }
}

fn most_resources(a: &Job, b: &Job) -> Ordering {
    if a.res > b.res {
        return Ordering::Less;
    } else {
        return Ordering::Greater
    }
}

fn greatest_area(a: &Job, b: &Job) -> Ordering {
    if (a.res as f64 * a.walltime) > (b.res as f64 * b.walltime) {
        return Ordering::Less;
    } else {
        return Ordering::Greater
    }
}
// Functions to get if a newly submitted job impact a running job
fn threshold_filter_policy(policy :ThresholdPolicy, a: &Job, b: &Job) -> bool {
    match policy {
        ThresholdPolicy::Count => {
            true
        },
        ThresholdPolicy::Area => {
            a.walltime * a.res as f64 >= b.walltime * b.res as f64
        },
        ThresholdPolicy::Walltime => {
            a.walltime >= b.walltime
        },
        ThresholdPolicy::Resources => {
            a.res as f64 >= b.res as f64
        },
    }
}

// Functions to get the pressure according to the current policy
fn threshold_policy(policy :ThresholdPolicy, job: &Job) -> f64 {
    match policy {
        ThresholdPolicy::Count => {
            1f64
        },
        ThresholdPolicy::Area => {
            job.walltime * job.res as f64
        },
        ThresholdPolicy::Walltime => {
            job.walltime
        },
        ThresholdPolicy::Resources => {
            job.res as f64
        },
    }
}

