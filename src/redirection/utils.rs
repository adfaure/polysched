use batsim::Job;
use std::str::FromStr;


#[derive(Clone, Debug)]
pub enum RedirectionPolicy {
    MaxWallTime,
    MinWallTime,
    MaxRes,
    MinRes,
}

impl FromStr for RedirectionPolicy {
    type Err = ();

    fn from_str(s: &str) -> Result<RedirectionPolicy, ()> {
        match s {
            "MaxWallTime" => Ok(RedirectionPolicy::MaxWallTime),
            "MinWallTime" => Ok(RedirectionPolicy::MinWallTime),
            "MaxRes" => Ok(RedirectionPolicy::MaxRes),
            "MinRes" => Ok(RedirectionPolicy::MinRes),
            _ => Err(()),
        }
    }
}

/// Function that given an input job
/// construct the same job, but with
/// the workload "rej".
/// The function could be improve
/// to take the name of the new workload
/// as input too.
pub fn construct_rejected_job(job: Job) -> Job {
    let (_, job_id) = Job::split_id(&job.id);

    let mut resub_job: Job = job.clone();
    resub_job.id = format!("rej!{}", job_id);
    trace!("{:?}", resub_job);
    resub_job
}

pub fn waiting_time(job: &Job, now: f64) -> f64 {
    now - job.subtime
}

#[allow(dead_code)]
pub fn stretch(job: &Job, now: f64) -> f64 {
    waiting_time(job, now) / job.walltime as f64
}

#[allow(dead_code)]
pub fn bounded_slowdown(job: &Job, now: f64, bound: f64) -> f64 {
    1_f64.max(waiting_time(job, now) / bound.max(job.walltime))
}
